# Simple GUI Demo

This project is a simple calculator for working with polynomials. The PySimpleGUI library is used to create a graphical interface.

### The `Polynomial` class:

- `__init__(self, degree, coefficients)`: Class constructor. Accepts the degree of a polynomial(`degree') and its coefficients (`coefficients').

- `Calculate(self, argument)`: A method for calculating the value of a polynomial for a given argument.

- `Print(self, argument)`: A method for formatted output of a polynomial.

### Functions `Sum`, `Dif`, `Mul`:

- `Sum(lhs, rhs)': Addition of polynomials.

- `Dif(lhs, rhs)': Subtraction of polynomials.

- `Mul(lhs, rhs)`: Multiplication of polynomials.

### PySimpleGUI GUI:

- The "Black" style is used.

- The user can enter polynomials and arguments.

- Calculation results are displayed in the output window.

- Implemented buttons for operations of addition, subtraction and multiplication of polynomials.

### Main loop `main()`:

- Initializes the PySimpleGUI window.

- Handles events such as input of polynomials, arguments, and operations.

- Performs the appropriate operations and displays the result in the output window.

- Updates the input fields after each event.

- Completes the loop when the window is closed.

This project provides an example of using PySimpleGUI to create a simple graphical interface and implement operations with polynomials.
