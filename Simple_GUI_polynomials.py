import PySimpleGUI as sg

class Polynomial(object):
 
    def __init__(self, degree, coefficients ):
        self.degree_ = degree
        self.coefficients_ = coefficients

    def Calculate(self, argument):
        coef_len = len(self.coefficients_) - 1
        result = 0 
        for i in range(coef_len, -1, -1):
            if(i == 0):
                result += self.coefficients_[coef_len - i]
            else:
                result += self.coefficients_[coef_len - i] * argument**i
        return result
    
    def Print(self, argument):
        coef_len = len(self.coefficients_) - 1
        result = ""
        flag = False
        for i in range(coef_len, -1, -1):
            if(self.coefficients_[coef_len - i] > 0):
                if(flag == False):
                    if(self.coefficients_[coef_len - i] == 1):
                        result += argument + "^" + str(i)
                    else:
                        result += str(self.coefficients_[coef_len - i]) + "*" + argument + "^" + str(i)
                    flag = True
                else:
                    if(i == 0):
                        result += " + " + str(self.coefficients_[coef_len - i])
                    elif(self.coefficients_[coef_len - i] == 1):
                        if(i == 1):
                            result += " + " + argument
                        else:
                            result += " + " + argument + "^" + str(i)
                    else:
                        if(i == 1):
                            result += " + " + str(self.coefficients_[coef_len - i]) + "*" + argument
                        else:
                            result += " + " + str(self.coefficients_[coef_len - i]) + "*" + argument + "^" + str(i)
            elif(self.coefficients_[coef_len - i] < 0):
                if(flag == False):
                    if(self.coefficients_[coef_len - i] == -1):
                        result += "-" + argument + "^" + str(i)
                    else:
                        result += "-" + str(self.coefficients_[coef_len - i] * -1) + "*" + argument + "^" + str(i)   
                    flag = True
                else:
                    if(i == 0):
                        result += " - " + str(self.coefficients_[coef_len - i] * -1)
                    elif(self.coefficients_[coef_len - i] == -1):   
                        if(i == 1):
                            result += " - " + argument
                        else:
                            result += " - " + argument + "^" + str(i)
                    else:
                        if(i == 1):
                            result += " - " + str(self.coefficients_[coef_len - i] * -1) + "*" + argument
                        else:
                            result += " - " + str(self.coefficients_[coef_len - i] * -1) + "*" + argument + "^" + str(i)
            else:
                result += " + 0"
                flag = True
        if(4 * result.count(" + 0") == len(result)):
            return "0"
        return result

def Sum(lhs, rhs):
    lhs_len = len(lhs.coefficients_)
    rhs_len = len(rhs.coefficients_)

    rhs_coefficients = rhs.coefficients_
    lhs_coefficients = lhs.coefficients_

    if(lhs_len > rhs_len):
        rhs_coefficients = [0] * (lhs_len - rhs_len)
        rhs_coefficients.extend(rhs.coefficients_)

    if(lhs_len < rhs_len):
        lhs_coefficients = [0] * (rhs_len - lhs_len)
        lhs_coefficients.extend(lhs.coefficients_)

    _len = len(lhs_coefficients)
    result = [0] * _len

    for i in range(0, _len):
        result[i] += lhs_coefficients[i]
        result[i] += rhs_coefficients[i]
    return Polynomial(_len, result)

def Dif(lhs, rhs):
    lhs_len = len(lhs.coefficients_)
    rhs_len = len(rhs.coefficients_)

    rhs_coefficients = rhs.coefficients_
    lhs_coefficients = lhs.coefficients_

    if(lhs_len > rhs_len):
        rhs_coefficients = [0] * (lhs_len - rhs_len)
        rhs_coefficients.extend(rhs.coefficients_)

    if(lhs_len < rhs_len):
        lhs_coefficients = [0] * (rhs_len - lhs_len)
        lhs_coefficients.extend(lhs.coefficients_)

    _len = len(lhs_coefficients)
    result = [0] * _len
    
    for i in range(0, _len):
        result[i] = lhs_coefficients[i]
        result[i] -= rhs_coefficients[i]
    return Polynomial(_len, result)

def Mul(lhs, rhs):
    lhs_len = len(lhs.coefficients_)
    rhs_len = len(rhs.coefficients_)
    coef_count = lhs_len + rhs_len - 1
    result = [0] * coef_count
    for i in range(0, lhs_len):
        for j in range(0, rhs_len):
            result[i + j] += lhs.coefficients_[i] * rhs.coefficients_[j]
    return Polynomial(coef_count, result)

def main():
    pol = Polynomial
    sg.theme('Black')

    layout = [  [sg.Output(key="Info", size=(80, 25))],
                [sg.Text('Input polynomial', size=(21, 1)), sg.InputText(key='new_polynomial', size=(56, 1))],
                [sg.Button('Input', key='Add',size=(72, 2))],              
                [sg.Text('Input argument', size=(21, 1)), sg.InputText(key='new_argument', size=(56, 1))],
                [sg.Button('Calculate', key='Calculate',size=(72, 2))],
                [sg.Text('Input polynomial', size=(21, 1)), sg.InputText(key='o_polynomial', size=(56, 1))],
                [sg.Button('Calculate sum', key='Sum',size=(72, 2))],
                [sg.Button('Calculate dif', key='Dif',size=(72, 2))],
                [sg.Button('Calculate mul', key='Mul',size=(72, 2))],
                [sg.Button('Print polynomial', key='Print',size=(72, 2))] ]

    window = sg.Window('Polynomial calculator', layout, element_justification="center")

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED:
            break

        if event == 'Add':   
            try:
                coefficients = values["new_polynomial"].split(" ")
                coefficients = list(map(int, coefficients))
                pol = Polynomial(len(coefficients), coefficients)
                print(pol.Print("x"), "________________________________________________________________________________", sep="\n", end="\n\n")
            except Exception:
                print("Incorrect data", "________________________________________________________________________________", sep="\n", end="\n\n")

        if event == 'Calculate':
            try:
                result = pol.Calculate(int(values["new_argument"]))
                print(pol.Print(values["new_argument"]), "=", result)
                print("________________________________________________________________________________", end="\n\n")
            except Exception:
                print("Incorrect data", "________________________________________________________________________________", sep="\n", end="\n\n")

        if event == 'Sum':
            try:
                coefficients = values["o_polynomial"].split(" ")
                coefficients = list(map(int, coefficients))
                o_pol = Polynomial(len(coefficients), coefficients)
                result = Sum(pol, o_pol)
                print(pol.Print("x"), "+", o_pol.Print("x"), "=", result.Print("x"), sep="\n", end="\n")
                pol = result
                print("________________________________________________________________________________", end="\n\n")
            except Exception:
                print("Incorrect data", "________________________________________________________________________________", sep="\n", end="\n\n")

        if event == 'Dif':
            try:
                coefficients = values["o_polynomial"].split(" ")
                coefficients = list(map(int, coefficients))
                o_pol = Polynomial(len(coefficients), coefficients)
                result = Dif(pol, o_pol)
                print(pol.Print("x"), "-", o_pol.Print("x"), "=", result.Print("x"), sep="\n", end="\n")
                pol = result
                print("________________________________________________________________________________", end="\n\n")
            except Exception:
                print("Incorrect data", "________________________________________________________________________________", sep="\n", end="\n\n")

        if event == 'Mul':
            try:
                coefficients = values["o_polynomial"].split(" ")
                coefficients = list(map(int, coefficients))
                o_pol = Polynomial(len(coefficients), coefficients)
                result = Mul(pol, o_pol)
                print(pol.Print("x"), "*", o_pol.Print("x"), "=", result.Print("x"), sep="\n", end="\n")
                pol = result
                print("________________________________________________________________________________", end="\n\n")
            except Exception:
                print("Incorrect data", "________________________________________________________________________________", sep="\n", end="\n\n")

        if event == 'Print':
            print(pol.Print("x"))
            print("________________________________________________________________________________", end="\n\n")    

        window.find_element('new_polynomial').Update('')
        window.find_element('new_argument').Update('')
        window.find_element('o_polynomial').Update('')

    window.close()

main()